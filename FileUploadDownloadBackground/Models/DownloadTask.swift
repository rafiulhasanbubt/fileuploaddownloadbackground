//
//  DownloadTask.swift
//  FileUploadDownloadBackground
//
//  Created by rafiul hasan on 2/4/22.
//

import Foundation

class DownloadTask {
    var file: File
    var url: URL?
    var inProgress = false
    var resumeData: Data?
    var task: URLSessionDownloadTask?
    
    init(file: File) {
        self.file = file
        self.url = URL(string: file.link)
    }
}
