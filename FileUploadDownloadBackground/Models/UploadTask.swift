//
//  UploadTask.swift
//  FileUploadDownloadBackground
//
//  Created by rafiul hasan on 2/4/22.
//

import Foundation

class UploadTask {
  var file: File
  var inProgress = false
  var task: URLSessionUploadTask?

    init(file: File) {
    self.file = file
  }
}
